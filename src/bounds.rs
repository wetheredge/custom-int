use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> Bounded for CustomInt<N, BITS> {
    #[inline]
    fn min_value() -> Self {
        Self(N::min_value() >> (N::BITS - <u32 as From<u8>>::from(BITS)))
    }

    #[inline]
    fn max_value() -> Self {
        Self(N::max_value() >> (N::BITS - <u32 as From<u8>>::from(BITS)))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    type I1 = CustomInt<i8, 1>;
    type I4 = CustomInt<i8, 4>;
    type I8 = CustomInt<i8, 8>;
    type U1 = CustomInt<u8, 1>;
    type U4 = CustomInt<u8, 4>;
    type U8 = CustomInt<u8, 8>;

    #[test]
    fn max() {
        assert_eq!(0, I1::max_value().get());
        assert_eq!(0x7, I4::max_value().get());
        assert_eq!(0x7F, I8::max_value().get());
        assert_eq!(1, U1::max_value().get());
        assert_eq!(0xF, U4::max_value().get());
        assert_eq!(0xFF, U8::max_value().get());
    }

    #[test]
    fn min() {
        assert_eq!(-1, I1::min_value().get());
        assert_eq!(-0x8, I4::min_value().get());
        assert_eq!(-0x80, I8::min_value().get());
        assert_eq!(0, U1::min_value().get());
        assert_eq!(0, U4::min_value().get());
        assert_eq!(0, U8::min_value().get());
    }
}
