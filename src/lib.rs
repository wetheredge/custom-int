#![cfg_attr(not(feature = "std"), no_std)]

mod bits;
mod bounds;
mod cast;
mod conversions;
mod identities;
mod ops;
mod sign;
mod sign_conversions;
mod storage;

// TODO: mod pow;

pub use bits::Bits;
pub use num_traits;
pub use sign_conversions::{ToSigned, ToUnsigned};
pub use storage::Storage;

use num_traits::*;

// TODO: restrict range of BITS
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CustomInt<N: Storage, const BITS: u8>(N);

impl<N: Storage, const BITS: u8> CustomInt<N, BITS> {
    #[inline]
    #[must_use]
    pub const unsafe fn new_unchecked(n: N) -> Self {
        Self(n)
    }

    #[inline]
    pub fn get(self) -> N {
        self.0
    }

    #[inline]
    #[must_use]
    pub fn new(n: N) -> Self {
        if let Some(n) = Self::new_checked(n) {
            n
        } else {
            panic!("invalid value for CustomInt<_, {BITS}>");
        }
    }

    #[inline]
    #[must_use]
    pub fn new_checked(n: N) -> Option<Self> {
        if n > Self::max_value().0 || n < Self::min_value().0 {
            None
        } else {
            Some(Self(n))
        }
    }

    #[inline]
    #[must_use]
    pub fn new_saturating(n: N) -> Self {
        if n > Self::max_value().0 {
            Self::max_value()
        } else if n < Self::min_value().0 {
            Self::min_value()
        } else {
            Self(n)
        }
    }
}

impl<N, const BITS: u8> PrimInt for CustomInt<N, BITS>
where
    N: Storage,
    Self: CheckedShr + WrappingShr + CheckedShl + WrappingShl,
{
    fn count_ones(self) -> u32 {
        todo!()
    }

    fn count_zeros(self) -> u32 {
        todo!()
    }

    fn leading_zeros(self) -> u32 {
        todo!()
    }

    fn trailing_zeros(self) -> u32 {
        todo!()
    }

    fn rotate_left(self, _n: u32) -> Self {
        todo!()
    }

    fn rotate_right(self, _n: u32) -> Self {
        todo!()
    }

    fn signed_shl(self, _n: u32) -> Self {
        todo!()
    }

    fn signed_shr(self, _n: u32) -> Self {
        todo!()
    }

    fn unsigned_shl(self, _n: u32) -> Self {
        todo!()
    }

    fn unsigned_shr(self, _n: u32) -> Self {
        todo!()
    }

    fn swap_bytes(self) -> Self {
        todo!()
    }

    fn from_be(_x: Self) -> Self {
        todo!()
    }

    fn from_le(_x: Self) -> Self {
        todo!()
    }

    fn to_be(self) -> Self {
        todo!()
    }

    fn to_le(self) -> Self {
        todo!()
    }

    fn pow(self, _exp: u32) -> Self {
        todo!()
    }
}

pub enum ParseCustomIntError<T> {
    PosOverflow,
    NegOverflow,
    Inner(T),
}

impl<N: Storage, const BITS: u8> Num for CustomInt<N, BITS> {
    type FromStrRadixErr = ParseCustomIntError<N::FromStrRadixErr>;

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        match N::from_str_radix(str, radix) {
            Ok(n) if n > N::max_value() => Err(ParseCustomIntError::PosOverflow),
            Ok(n) if n < N::min_value() => Err(ParseCustomIntError::NegOverflow),
            Ok(n) => Ok(Self(n)),
            Err(e) => Err(ParseCustomIntError::Inner(e)),
        }
    }
}
