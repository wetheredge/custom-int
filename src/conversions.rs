use crate::CustomInt;

macro_rules! impl_from {
    ($from:ty, $bits:expr, [$( $to:ty )+]) => {
        $(
            impl<const BITS: u8> From<CustomInt<$from, BITS>> for $to {
                #[inline]
                fn from(n: CustomInt<$from, BITS>) -> Self {
                    n.0.into()
                }
            }

            impl From<$from> for CustomInt<$to, $bits> {
                #[inline]
                fn from(n: $from) -> Self {
                    Self(n.into())
                }
            }
        )+
    };
}

impl_from!(i8, 8, [i8 i16 i32 i64]);
impl_from!(u8, 8, [u8 u16 u32 u64]);
impl_from!(i16, 16, [i16 i32 i64]);
impl_from!(u16, 16, [u16 u32 u64]);
impl_from!(i32, 32, [i32 i64]);
impl_from!(u32, 32, [u32 u64]);
impl_from!(i64, 64, [i64]);
impl_from!(u64, 64, [u64]);

#[cfg(feature = "x128")]
mod x128 {
    use super::*;

    impl_from!(i8, 8, [i128]);
    impl_from!(u8, 8, [u128]);
    impl_from!(i16, 16, [i128]);
    impl_from!(u16, 16, [u128]);
    impl_from!(i32, 32, [i128]);
    impl_from!(u32, 32, [u128]);
    impl_from!(i64, 64, [i128]);
    impl_from!(u64, 64, [u128]);
    impl_from!(i128, 128, [i128]);
    impl_from!(u128, 128, [u128]);
}
