use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> Signed for CustomInt<N, BITS>
where
    Self: Zero + One + Bounded + PartialOrd + PartialEq + Copy,
{
    fn abs(&self) -> Self {
        if *self > Self::zero() || *self == Self::min_value() {
            *self
        } else {
            -*self
        }
    }

    fn abs_sub(&self, rhs: &Self) -> Self {
        let diff = *self - *rhs;
        if diff.is_positive() {
            diff
        } else {
            Self::zero()
        }
    }

    fn signum(&self) -> Self {
        if self.is_positive() {
            Self::one()
        } else if self.is_negative() {
            -Self::one()
        } else {
            Self::zero()
        }
    }

    fn is_positive(&self) -> bool {
        *self > Self::zero()
    }

    fn is_negative(&self) -> bool {
        *self < Self::zero()
    }
}

impl<N: Storage, const BITS: u8> Unsigned for CustomInt<N, BITS> {}
