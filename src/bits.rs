use crate::{CustomInt, Storage};

pub trait Bits {
    const BITS: u32;
}

impl<N: Storage, const BITS: u8> Bits for CustomInt<N, BITS> {
    const BITS: u32 = BITS as u32;
}

macro_rules! impl_bits {
    ($( $t:ty ),+) => {
        $(
            impl $crate::Bits for $t {
                const BITS: u32 = Self::BITS;
            }
        )+
    }
}

impl_bits!(i8, u8, i16, u16, i32, u32, i64, u64);
#[cfg(feature = "x128")]
impl_bits!(i128, u128);
