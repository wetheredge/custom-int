use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> One for CustomInt<N, BITS> {
    #[inline]
    fn one() -> Self {
        Self(N::one())
    }
}

impl<N: Storage, const BITS: u8> Zero for CustomInt<N, BITS> {
    #[inline]
    fn zero() -> Self {
        Self(N::zero())
    }

    #[inline]
    fn is_zero(&self) -> bool {
        self.0.is_zero()
    }
}
