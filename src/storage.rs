use crate::Bits;
use core::hash::Hash;
use num_traits::*;

pub trait Storage:
    seal::Sealed
    + Copy
    + Eq
    + Ord
    + Hash
    + PrimInt
    + Num
    + Bits
    + Bounded
    + CheckedRem
    + CheckedNeg
    + CheckedShl
    + CheckedShr
    + SaturatingMul
    + WrappingAdd
    + WrappingSub
    + WrappingMul
{
}

mod seal {
    pub trait Sealed {}

    impl Sealed for i8 {}
    impl Sealed for u8 {}
    impl Sealed for i16 {}
    impl Sealed for u16 {}
    impl Sealed for i32 {}
    impl Sealed for u32 {}
    impl Sealed for i64 {}
    impl Sealed for u64 {}
    #[cfg(feature = "x128")]
    impl Sealed for i128 {}
    #[cfg(feature = "x128")]
    impl Sealed for u128 {}
}

impl Storage for i8 {}
impl Storage for u8 {}
impl Storage for i16 {}
impl Storage for u16 {}
impl Storage for i32 {}
impl Storage for u32 {}
impl Storage for i64 {}
impl Storage for u64 {}
#[cfg(feature = "x128")]
impl Storage for i128 {}
#[cfg(feature = "x128")]
impl Storage for u128 {}
