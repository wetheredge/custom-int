use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> NumCast for CustomInt<N, BITS> {
    #[inline]
    fn from<T: ToPrimitive>(_n: T) -> Option<Self> {
        todo!()
    }
}

impl<N: Storage, const BITS: u8> FromPrimitive for CustomInt<N, BITS> {
    #[inline]
    fn from_isize(n: isize) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_i8(n: i8) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_i16(n: i16) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_i32(n: i32) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_i64(n: i64) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[cfg(feature = "x128")]
    #[inline]
    fn from_i128(n: i128) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_usize(n: usize) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_u8(n: u8) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_u16(n: u16) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_u32(n: u32) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[inline]
    fn from_u64(n: u64) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }

    #[cfg(feature = "x128")]
    #[inline]
    fn from_u128(n: u128) -> Option<Self> {
        N::from(n).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> ToPrimitive for CustomInt<N, BITS> {
    #[inline]
    fn to_isize(&self) -> Option<isize> {
        self.0.to_isize()
    }

    #[inline]
    fn to_i8(&self) -> Option<i8> {
        self.0.to_i8()
    }

    #[inline]
    fn to_i16(&self) -> Option<i16> {
        self.0.to_i16()
    }

    #[inline]
    fn to_i32(&self) -> Option<i32> {
        self.0.to_i32()
    }

    #[inline]
    fn to_i64(&self) -> Option<i64> {
        self.0.to_i64()
    }

    #[cfg(feature = "x128")]
    #[inline]
    fn to_i128(&self) -> Option<i128> {
        self.0.to_i128()
    }

    #[inline]
    fn to_usize(&self) -> Option<usize> {
        self.0.to_usize()
    }

    #[inline]
    fn to_u8(&self) -> Option<u8> {
        self.0.to_u8()
    }

    #[inline]
    fn to_u16(&self) -> Option<u16> {
        self.0.to_u16()
    }

    #[inline]
    fn to_u32(&self) -> Option<u32> {
        self.0.to_u32()
    }

    #[inline]
    fn to_u64(&self) -> Option<u64> {
        self.0.to_u64()
    }

    #[cfg(feature = "x128")]
    #[inline]
    fn to_u128(&self) -> Option<u128> {
        self.0.to_u128()
    }
}
