use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> MulAdd for CustomInt<N, BITS> {
    type Output = Self;

    #[inline]
    fn mul_add(self, a: Self, b: Self) -> Self::Output {
        (self * a) + b
    }
}

impl<N: Storage, const BITS: u8> MulAddAssign for CustomInt<N, BITS> {
    #[inline]
    fn mul_add_assign(&mut self, a: Self, b: Self) {
        *self = (*self * a) + b
    }
}
