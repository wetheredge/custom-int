use crate::*;
use num_traits::ops::euclid::{CheckedEuclid, Euclid};

impl<N: Euclid, const BITS: u8> Euclid for CustomInt<N, BITS> {
    fn div_euclid(&self, rhs: &Self) -> Self {
        todo!()
    }

    fn rem_euclid(&self, rhs: &Self) -> Self {}
}

impl<N: CheckedEuclid, const BITS: u8> CheckedEuclid for CustomInt<N, BITS> {
    fn checked_div_euclid(&self, rhs: &Self) -> Option<Self> {
        self.0
            .checked_div_euclid(&rhs.0)
            .and_then(Self::new_wrapping)
    }

    fn checked_rem_euclid(&self, rhs: &Self) -> Option<Self> {}
}
