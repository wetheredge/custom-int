use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> Saturating for CustomInt<N, BITS> {
    fn saturating_add(self, rhs: Self) -> Self {
        <Self as SaturatingAdd>::saturating_add(&self, &rhs)
    }

    fn saturating_sub(self, rhs: Self) -> Self {
        <Self as SaturatingSub>::saturating_sub(&self, &rhs)
    }
}

impl<N: Storage, const BITS: u8> SaturatingAdd for CustomInt<N, BITS> {
    fn saturating_add(&self, rhs: &Self) -> Self {
        Self::new_saturating(self.0.saturating_add(rhs.0))
    }
}

impl<N: Storage, const BITS: u8> SaturatingSub for CustomInt<N, BITS> {
    fn saturating_sub(&self, rhs: &Self) -> Self {
        Self::new_saturating(self.0.saturating_sub(rhs.0))
    }
}

impl<N: Storage, const BITS: u8> SaturatingMul for CustomInt<N, BITS> {
    fn saturating_mul(&self, rhs: &Self) -> Self {
        Self::new_saturating(self.0.saturating_mul(&rhs.0))
    }
}
