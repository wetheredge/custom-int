use crate::{CustomInt, Storage};
use num_traits::*;

impl<N: Storage, const BITS: u8> CheckedAdd for CustomInt<N, BITS> {
    #[inline]
    fn checked_add(&self, rhs: &Self) -> Option<Self> {
        self.0.checked_add(&rhs.0).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedSub for CustomInt<N, BITS> {
    #[inline]
    fn checked_sub(&self, rhs: &Self) -> Option<Self> {
        self.0.checked_sub(&rhs.0).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedMul for CustomInt<N, BITS> {
    #[inline]
    fn checked_mul(&self, rhs: &Self) -> Option<Self> {
        self.0.checked_mul(&rhs.0).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedDiv for CustomInt<N, BITS> {
    #[inline]
    fn checked_div(&self, rhs: &Self) -> Option<Self> {
        self.0.checked_div(&rhs.0).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedRem for CustomInt<N, BITS> {
    #[inline]
    fn checked_rem(&self, rhs: &Self) -> Option<Self> {
        self.0.checked_rem(&rhs.0).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedNeg for CustomInt<N, BITS> {
    #[inline]
    fn checked_neg(&self) -> Option<Self> {
        self.0.checked_neg().and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedShl for CustomInt<N, BITS> {
    #[inline]
    fn checked_shl(&self, rhs: u32) -> Option<Self> {
        self.0.checked_shl(rhs).and_then(Self::new_checked)
    }
}

impl<N: Storage, const BITS: u8> CheckedShr for CustomInt<N, BITS> {
    #[inline]
    fn checked_shr(&self, rhs: u32) -> Option<Self> {
        self.0.checked_shr(rhs).and_then(Self::new_checked)
    }
}
