pub mod checked;
// TODO: pub mod euclid;
pub mod mul_add;
pub mod overflowing;
pub mod saturating;
pub mod wrapping;

use crate::{CustomInt, Storage};
use core::ops::{Add, BitAnd, BitOr, BitXor, Div, Mul, Neg, Not, Rem, Shl, Shr, Sub};
use num_traits::*;

impl<N: Storage, const BITS: u8> Add<Self> for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn add(self, rhs: Self) -> Self::Output {
        self.checked_add(&rhs).unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn add(self, rhs: Self) -> Self::Output {
        self.wrapping_add(rhs)
    }
}

impl<N: Storage, const BITS: u8> Sub<Self> for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn sub(self, rhs: Self) -> Self::Output {
        self.checked_sub(&rhs).unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn sub(self, rhs: Self) -> Self::Output {
        self.wrapping_sub(&rhs)
    }
}

impl<N: Storage, const BITS: u8> Mul<Self> for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn mul(self, rhs: Self) -> Self::Output {
        self.checked_mul(&rhs).unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn mul(self, rhs: Self) -> Self::Output {
        self.wrapping_mul(&rhs)
    }
}

impl<N: Storage, const BITS: u8> Div<Self> for CustomInt<N, BITS> {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        Self::new(self.0.div(rhs.0))
    }
}

impl<N: Storage, const BITS: u8> Rem<Self> for CustomInt<N, BITS> {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        Self::new(self.0.rem(rhs.0))
    }
}

impl<N: Storage, const BITS: u8> Neg for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn neg(self) -> Self::Output {
        self.checked_neg().unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn neg(self) -> Self::Output {
        self.wrapping_neg()
    }
}

impl<N: Storage, const BITS: u8> Not for CustomInt<N, BITS> {
    type Output = Self;

    fn not(self) -> Self::Output {
        Self(!self.0)
    }
}

impl<N: Storage, const BITS: u8> BitAnd for CustomInt<N, BITS> {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        Self(self.0 & rhs.0)
    }
}

impl<N: Storage, const BITS: u8> BitOr for CustomInt<N, BITS> {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        Self(self.0 | rhs.0)
    }
}

impl<N: Storage, const BITS: u8> BitXor for CustomInt<N, BITS> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        Self(self.0 ^ rhs.0)
    }
}

impl<N: Storage, const BITS: u8> Shl<u32> for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn shl(self, rhs: u32) -> Self::Output {
        self.checked_shl(rhs).unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn shl(self, rhs: u32) -> Self::Output {
        self.wrapping_shl(rhs)
    }
}

impl<N: Storage, const BITS: u8> Shr<u32> for CustomInt<N, BITS> {
    type Output = Self;

    #[cfg(debug_assertions)]
    fn shr(self, rhs: u32) -> Self::Output {
        self.checked_shr(rhs).unwrap()
    }

    #[cfg(not(debug_assertions))]
    fn shr(self, rhs: u32) -> Self::Output {
        self.wrapping_shr(rhs)
    }
}

impl<N: Storage, const BITS: u8> Shl<usize> for CustomInt<N, BITS> {
    type Output = Self;

    fn shl(self, _rhs: usize) -> Self::Output {
        todo!()
    }
}

impl<N: Storage, const BITS: u8> Shr<usize> for CustomInt<N, BITS> {
    type Output = Self;

    fn shr(self, _rhs: usize) -> Self::Output {
        todo!()
    }
}
