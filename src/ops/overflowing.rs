use crate::{CustomInt, Storage};
use num_traits::ops::overflowing::{OverflowingAdd, OverflowingMul, OverflowingSub};
use num_traits::*;

impl<N: Storage, const BITS: u8> OverflowingAdd for CustomInt<N, BITS> {
    fn overflowing_add(&self, rhs: &Self) -> (Self, bool) {
        if let Some(sum) = self.checked_add(rhs) {
            (sum, false)
        } else {
            (self.wrapping_add(rhs), true)
        }
    }
}

impl<N: Storage, const BITS: u8> OverflowingSub for CustomInt<N, BITS> {
    fn overflowing_sub(&self, rhs: &Self) -> (Self, bool) {
        if let Some(sum) = self.checked_sub(rhs) {
            (sum, false)
        } else {
            (self.wrapping_sub(rhs), true)
        }
    }
}

impl<N: Storage, const BITS: u8> OverflowingMul for CustomInt<N, BITS> {
    fn overflowing_mul(&self, rhs: &Self) -> (Self, bool) {
        if let Some(sum) = self.checked_mul(rhs) {
            (sum, false)
        } else {
            (self.wrapping_mul(rhs), true)
        }
    }
}
