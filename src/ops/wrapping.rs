use crate::{CustomInt, Storage};
use num_traits::*;

fn from_wrapped<N: Storage, const BITS: u8>(n: N) -> CustomInt<N, BITS> {
    let max = CustomInt::<N, BITS>::max_value().0;
    let min = CustomInt::<N, BITS>::min_value().0;

    let n = if n > max {
        max - (N::max_value() - n)
    } else if n < min {
        min - (n - N::min_value())
    } else {
        n
    };

    CustomInt(n)
}

impl<N: Storage, const BITS: u8> WrappingAdd for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_add(&self, rhs: &Self) -> Self {
        let sum = self.0.wrapping_add(&rhs.0);
        from_wrapped(sum)
    }
}

impl<N: Storage, const BITS: u8> WrappingSub for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_sub(&self, rhs: &Self) -> Self {
        let diff = self.0.wrapping_sub(&rhs.0);
        from_wrapped(diff)
    }
}

impl<N: Storage, const BITS: u8> WrappingMul for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_mul(&self, rhs: &Self) -> Self {
        let product = self.0.wrapping_mul(&rhs.0);
        from_wrapped(product)
    }
}

impl<N: Storage, const BITS: u8> WrappingNeg for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_neg(&self) -> Self {
        if let Some(neg) = self.checked_neg() {
            neg
        } else {
            *self
        }
    }
}

impl<N: Storage, const BITS: u8> WrappingShl for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_shl(&self, _rhs: u32) -> Self {
        todo!()
    }
}

impl<N: Storage, const BITS: u8> WrappingShr for CustomInt<N, BITS> {
    #[inline]
    fn wrapping_shr(&self, _rhs: u32) -> Self {
        todo!()
    }
}
