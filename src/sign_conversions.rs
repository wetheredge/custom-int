use crate::{Bits, CustomInt};
use num_traits::AsPrimitive;

pub trait ToSigned: Sized {
    type Signed;
    fn to_signed(self) -> Self::Signed;
}

pub trait ToUnsigned: Sized {
    type Unsigned;
    fn to_unsigned(self) -> Self::Unsigned;
}

macro_rules! impl_signs {
    ($signed:ty, $unsigned:ty) => {
        impl<const BITS: u8> ToSigned for CustomInt<$unsigned, BITS> {
            type Signed = CustomInt<$signed, BITS>;

            #[inline]
            fn to_signed(self) -> Self::Signed {
                let extra_bits: u32 = <$unsigned>::BITS - Self::BITS;
                let unsigned = self.0 << extra_bits;
                let signed = <$unsigned as AsPrimitive<$signed>>::as_(unsigned) >> extra_bits;
                CustomInt(signed)
            }
        }

        impl<const BITS: u8> ToUnsigned for CustomInt<$signed, BITS> {
            type Unsigned = CustomInt<$unsigned, BITS>;

            #[inline]
            fn to_unsigned(self) -> Self::Unsigned {
                let extra_bits: u32 = <$signed>::BITS - Self::BITS;
                dbg!(extra_bits);
                dbg!(self.0);
                let unsigned = self.0 as $unsigned;
                dbg!(unsigned);
                let unsigned = unsigned << extra_bits;
                CustomInt(unsigned >> extra_bits)
            }
        }
    };
}

impl_signs!(i8, u8);
impl_signs!(i16, u16);
impl_signs!(i32, u32);
impl_signs!(i64, u64);
#[cfg(feature = "x128")]
impl_signs!(i128, u128);

#[cfg(test)]
mod test {
    use super::*;
    use num_traits::Bounded;

    #[test]
    fn to_signed_full_width() {
        assert_eq!(-1_i8, CustomInt::<u8, 8>::max_value().to_signed().get());
    }

    #[test]
    fn to_signed_partial_width() {
        assert_eq!(-1_i8, CustomInt::<u8, 2>::max_value().to_signed().get());
    }

    #[test]
    fn to_unsigned_full_width() {
        assert_eq!(0xFF_u8, CustomInt::<i8, 8>::new(-1).to_unsigned().get());
    }

    #[test]
    fn to_unsigned_partial_width() {
        assert_eq!(3_u8, CustomInt::<i8, 2>::new(-1).to_unsigned().get());
    }
}
